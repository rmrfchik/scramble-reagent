(ns scramble-reagent.prod
  (:require [scramble-reagent.core :as core]))

;;ignore println statements in prod
(set! *print-fn* (fn [& _]))

(core/init!)
