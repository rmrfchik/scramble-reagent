(ns scramble-reagent.scramble)

(defn inc-freq
  [freqs symb]
  (let [freq (or (get freqs symb) 0)]
    (into freqs {symb (+ 1 freq)})))

(defn freqs
  [str]
  (reduce inc-freq {} (seq str)))

(defn check-freq
  [freqs symb minvalue]
  (let [freq (or (get freqs symb) 0)]
    (>= freq minvalue)))

(defn scramble?
  [allowed-chars target]
  (println "'" allowed-chars "' '" target "'")
  (let [avail-freq (freqs allowed-chars)
        targer-freq (freqs target)]
    (loop [targer-freq targer-freq]
      (let [pair (first targer-freq)
            [symb freq] pair]
        (or (empty? targer-freq)
            (and (check-freq avail-freq symb freq)
                 (recur (rest targer-freq))))))))

